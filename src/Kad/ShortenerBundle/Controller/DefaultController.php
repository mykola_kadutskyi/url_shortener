<?php

namespace Kad\ShortenerBundle\Controller;

use Kad\ShortenerBundle\Entity\Stats;
use Kad\ShortenerBundle\Entity\UrlPair;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


class DefaultController extends Controller {

	public function indexAction( Request $request ) {

		$urlPair = new UrlPair();

		$form = $this->createFormBuilder( $urlPair )
		             ->add( 'originalUrl', UrlType::class, array( 'label' => 'Original Url' ) )
		             ->add( 'save', SubmitType::class, array( 'label' => 'Shorten Url' ) )
		             ->getForm();

		$form->handleRequest( $request );

		if ( $form->isSubmitted() && $form->isValid() ) {

			$randomString = $this->get( 'kad_shortener.generate_string' );
			$shortUrl     = $randomString->generate();
			for ( $i = 0; $i == 1; $i ++ ) {
				if ( $randomString->checkInBD( $shortUrl ) ) {
					$shortUrl = $randomString->generate();
					$i --;
				}
			}


			$urlPair->setOriginalUrl( $form['originalUrl']->getData() );
			$urlPair->setShortUrl( $shortUrl );
			$interval = $this->container->getParameter( 'kad_url_shortener.remove_interval' );
			$date     = new \DateTime();
			$date->add( new \DateInterval( "P" . $interval['years'] . "Y" . $interval['months'] . "M" . $interval['days'] . "DT" . $interval['hours'] . "H" . $interval['minutes'] . "M" . $interval['seconds'] . "S" ) );
			$urlPair->setRemoveAt( $date );


			$em = $this->getDoctrine()->getManager();
			$em->persist( $urlPair );
			$em->flush();

			$em = $this->getDoctrine()->getManager();

			$repository = $em->getRepository( 'KadShortenerBundle:UrlPair' );
			$pair       = $repository->findOneBy( array( "shortUrl" => $shortUrl ) );
			$stat       = new Stats();
			$stat->setAction( "created" );
			$stat->setPairId( $pair->getId() );
			$em->persist( $stat );
			$em->flush();

			return $this->render( 'KadShortenerBundle:Default:success.html.twig', array(
				'short_url'  => $shortUrl,
				'origin_url' => $form['originalUrl']->getData(),
				'id'         => $urlPair->getId()
			) );
		}


		return $this->render( 'KadShortenerBundle:Default:index.html.twig', array(
			'form' => $form->createView(),
		) );

	}

	public function shortUrlRedirectAction( $short_url ) {
		$urlPair = $this->getDoctrine()
		                ->getRepository( 'KadShortenerBundle:UrlPair' )
		                ->findOneBy( array( 'shortUrl' => $short_url ) );

		if ( $urlPair ) {
			return $this->redirect( $urlPair->getOriginalUrl() );
		} else {
			throw $this->createNotFoundException( 'Page not found' );
		}

	}

	public function shortenUrlApiAction( Request $request, $_format ) {
		$url = $request->query->get( 'url' );

		$urlPair = new UrlPair();

		$randomString = $this->get( 'kad_shortener.generate_string' );
		$shortUrl     = $randomString->generate();
		for ( $i = 0; $i == 1; $i ++ ) {
			if ( $randomString->checkInBD( $shortUrl ) ) {
				$shortUrl = $randomString->generate();
				$i --;
			}
		}


		$urlPair->setOriginalUrl( $url );
		$urlPair->setShortUrl( $shortUrl );

		$em = $this->getDoctrine()->getManager();

		$em->persist( $urlPair );
		$em->flush();

		$response = new Response();

		$preparedShortUrl = $this->generateUrl( 'kad_short_url_redirect', array( 'short_url' => $urlPair->getShortUrl() ), UrlGeneratorInterface::ABSOLUTE_URL );

		if ( $_format == 'json' ) {
			$response->setContent( json_encode( array( 'url' => $preparedShortUrl ) ) );

			$response->headers->set( 'Content-Type', 'application/json' );

		} elseif ( $_format == 'xml' ) {
			$response->setContent( "<?xml version=\"1.0\" encoding=\"UTF-8\"?><shortener><url>" . $preparedShortUrl . "</url></shortener>" );

			$response->headers->set( 'Content-Type', 'text/xml' );

		}

		return $response;

	}

	public function editUrlAction( Request $request, $id ) {

		$urlPair = $this->getDoctrine()
		                ->getRepository( 'KadShortenerBundle:UrlPair' )
		                ->findOneById( $id );

		$form = $this->createFormBuilder( $urlPair )
		             ->add( 'shortUrl', TextType::class, array( 'label' => $request->getScheme() . '://' . $request->getHttpHost() . '/' ) )
		             ->add( 'save', SubmitType::class, array( 'label' => 'Change Short Url' ) )
		             ->getForm();

		$form->handleRequest( $request );

		if ( $form->isSubmitted() && $form->isValid() ) {


			$urlPair->setShortUrl( $form['shortUrl']->getData() );

			$em = $this->getDoctrine()->getManager();

			$em->persist( $urlPair );
			$em->flush();

			return $this->render( 'KadShortenerBundle:Default:success.html.twig', array(
				'short_url'  => $form['shortUrl']->getData(),
				'origin_url' => $urlPair->getOriginalUrl(),
				'id'         => $urlPair->getId()
			) );
		}

		return $this->render( 'KadShortenerBundle:Default:index.html.twig', array(
			'form' => $form->createView(),
		) );

	}

	public function shortenUrlReceiveJsonAction(Request $request) {

	}

}
