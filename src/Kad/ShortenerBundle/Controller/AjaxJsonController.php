<?php

namespace Kad\ShortenerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Kad\ShortenerBundle\Entity\Stats;
use Kad\ShortenerBundle\Entity\UrlPair;
use FOS\RestBundle\Controller\Annotations as Rest;


class AjaxJsonController extends Controller {

	/**
	 * @Rest\View()
	 */
	public function shortenUrlAction( Request $request ) {

		$result = array( 'errors' => 0 );

		$isAjax = $request->isXmlHttpRequest();
		if ( $isAjax ) {
			$url = $request->get( 'url' );

			$urlPair = new UrlPair();


			$randomString = $this->get( 'kad_shortener.generate_string' );
			$shortUrl     = $randomString->generate();
			for ( $i = 0; $i == 1; $i ++ ) {
				if ( $randomString->checkInBD( $shortUrl ) ) {
					$shortUrl = $randomString->generate();
					$i --;
				}
			}


			$urlPair->setOriginalUrl( $url );
			$urlPair->setShortUrl( $shortUrl );
			$interval = $this->container->getParameter( 'kad_url_shortener.remove_interval' );
			$date     = new \DateTime();
			$date->add( new \DateInterval( "P" . $interval['years'] . "Y" . $interval['months'] . "M" . $interval['days'] . "DT" . $interval['hours'] . "H" . $interval['minutes'] . "M" . $interval['seconds'] . "S" ) );
			$urlPair->setRemoveAt( $date );

			$validator = $this->get( 'validator' );
			$errors    = $validator->validate( $urlPair );

			if ( count( $errors ) > 0 ) {
				$result['data']   = $errors;
				$result['errors'] = count( $errors );
			} else {
				$em = $this->getDoctrine()->getManager();
				$em->persist( $urlPair );
				$em->flush();

				$em = $this->getDoctrine()->getManager();

				$repository = $em->getRepository( 'KadShortenerBundle:UrlPair' );
				$pair       = $repository->findOneBy( array( "shortUrl" => $shortUrl ) );
				$stat       = new Stats();
				$stat->setAction( "created" );
				$stat->setPairId( $pair->getId() );
				$em->persist( $stat );
				$em->flush();

				$result['data'] = $urlPair;
			}

		}

		return $result;
	}

	/**
	 * @Rest\View()
	 */
	public function editUrlAction( Request $request ) {
		$result = array( "errors" => 0 );
		$isAjax = $request->isXmlHttpRequest();
		if ( $isAjax ) {
			$id      = $request->get( 'id' );
			$shrtUrl = $request->get( 'short_url' );

			$urlPair = $this->getDoctrine()
			                ->getRepository( 'KadShortenerBundle:UrlPair' )
			                ->findOneById( $id );
			$urlPair->setShortUrl( $shrtUrl );

			$validator = $this->get( 'validator' );
			$errors    = $validator->validate( $urlPair );

			if ( count( $errors ) > 0 ) {
				$result['data']   = $errors;
				$result['id']     = $id;
				$result['errors'] = count( $errors );
			} else {
				$em = $this->getDoctrine()->getManager();

				$em->persist( $urlPair );
				$em->flush();
				$result['data'] = $urlPair;
			}

		}

		return $result;

	}

	/**
	 * @Rest\View()
	 */
	public function makeUrlImmortalAction( Request $request ) {

		$result = array( "errors" => 0 );

		$isAjax = $request->isXmlHttpRequest();
		if ( $isAjax ) {
			$id      = $request->get( 'id' );
			$urlPair = $this->getDoctrine()
			                ->getRepository( 'KadShortenerBundle:UrlPair' )
			                ->findOneById( $id );


			$interval = $this->container->getParameter( 'kad_url_shortener.immortal_interval' );
			$date     = new \DateTime();
			$date->add( new \DateInterval( "P" . $interval['years'] . "Y" . $interval['months'] . "M" . $interval['days'] . "DT" . $interval['hours'] . "H" . $interval['minutes'] . "M" . $interval['seconds'] . "S" ) );
			$urlPair->setRemoveAt( $date );

			$em = $this->getDoctrine()->getManager();
			$em->persist( $urlPair );
			$em->flush();

			$result['data'] = $urlPair;
		}

		return $result;

	}
}