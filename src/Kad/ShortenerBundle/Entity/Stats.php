<?php

namespace Kad\ShortenerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Stats
 *
 * @ORM\Table(name="stats")
 * @ORM\Entity(repositoryClass="Kad\ShortenerBundle\Repository\StatsRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Stats
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="string", length=255)
     */
    private $action;

    /**
     * @var int
     *
     * @ORM\Column(name="pair_id", type="integer")
     */
    private $pairId;

    /**
     * @var string
     *
     * @ORM\Column(name="time", type="datetime")
     */
    private $time;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set action
     *
     * @param string $action
     *
     * @return Stats
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set pairId
     *
     * @param integer $pairId
     *
     * @return Stats
     */
    public function setPairId($pairId)
    {
        $this->pairId = $pairId;

        return $this;
    }

    /**
     * Get pairId
     *
     * @return int
     */
    public function getPairId()
    {
        return $this->pairId;
    }

    /**
     * @ORM\PrePersist
     */
    public function setTimeValue()
    {
        $this->time = new \DateTime();
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return Stats
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }
}
