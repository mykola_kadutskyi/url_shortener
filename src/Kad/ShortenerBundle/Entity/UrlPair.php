<?php

namespace Kad\ShortenerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Kad\ShortenerBundle\Validator\Constraints as ShortenerAssert;


/**
 * UrlPair
 *
 * @ORM\Table(name="url_pair")
 * @ORM\Entity(repositoryClass="Kad\ShortenerBundle\Repository\UrlPairRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class UrlPair {
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="original_url", type="text")
	 * @Assert\NotBlank()
	 * @Assert\Url(
	 *    message = "The url '{{ value }}' is not a valid url",
	 *    protocols = {"http", "https"},
	 * )
	 */
	private $originalUrl;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="short_url", type="text")
	 *
	 * @Assert\NotBlank()
	 * @Assert\Length(
	 *      min = 5,
	 *      max = 10,
	 *      minMessage = "Your url string must be at least {{ limit }} characters long",
	 *      maxMessage = "Your url string cannot be longer than {{ limit }} characters"
	 * )
	 *
	 * @ShortenerAssert\ExistInDb
	 */
	private $shortUrl;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="created_at", type="datetime")
	 */
	private $createdAt;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="remove_at", type="datetime")
	 */
	private $removeAt;

	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set originalUrl
	 *
	 * @param string $originalUrl
	 *
	 * @return UrlPair
	 */
	public function setOriginalUrl( $originalUrl ) {
		$this->originalUrl = $originalUrl;

		return $this;
	}

	/**
	 * Get originalUrl
	 *
	 * @return string
	 */
	public function getOriginalUrl() {
		return $this->originalUrl;
	}

	/**
	 * Set shortUrl
	 *
	 * @param string $shortUrl
	 *
	 * @return UrlPair
	 */
	public function setShortUrl( $shortUrl ) {
		$this->shortUrl = $shortUrl;

		return $this;
	}

	/**
	 * Get shortUrl
	 *
	 * @return string
	 */
	public function getShortUrl() {
		return $this->shortUrl;
	}

	/**
	 * @ORM\PrePersist
	 */
	public function setCreatedAtValue() {
		$this->createdAt = new \DateTime();

	}


	/**
	 * Set createdAt
	 *
	 * @param \DateTime $createdAt
	 *
	 * @return UrlPair
	 */
	public function setCreatedAt( $createdAt ) {
		$this->createdAt = $createdAt;

		return $this;
	}

	/**
	 * Get createdAt
	 *
	 * @return \DateTime
	 */
	public function getCreatedAt() {
		return $this->createdAt;
	}

	/**
	 * Set removedAt
	 *
	 * @param \DateTime $removeAt
	 *
	 * @return UrlPair
	 */
	public function setRemoveAt( $removeAt ) {
		$this->removeAt = $removeAt;

		return $this;
	}

	/**
	 * Get removedAt
	 *
	 * @return \DateTime
	 */
	public function getRemoveAt() {
		return $this->removeAt;
	}
}
