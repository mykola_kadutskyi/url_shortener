<?php
namespace Kad\ShortenerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RemoveFromDbCommand extends ContainerAwareCommand {
	protected function configure() {
		$this
			->setName( 'database:remove:needless' )
			->setDescription( 'Remove needless records from db' );

	}

	protected function execute( InputInterface $input, OutputInterface $output ) {
		$remover = $this->getContainer()->get( 'kad_shortener.remove_needless' );

		$remover->checkAndRemoveFromDb();
//		for($i = count($a); $i ){
//
//		}
//		$output->writeln( $a );

	}
}