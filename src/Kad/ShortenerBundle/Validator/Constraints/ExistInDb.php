<?php
namespace Kad\ShortenerBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ExistInDb extends Constraint {
	public $message = 'The url string "%string%" already exists in database.';

	public function validatedBy()
	{
		return 'exist_in_db_val';
	}
}
