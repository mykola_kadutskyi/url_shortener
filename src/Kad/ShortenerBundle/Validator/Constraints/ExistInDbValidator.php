<?php

namespace Kad\ShortenerBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ExistInDbValidator extends ConstraintValidator {

	public $generator;

	public function validate( $value, Constraint $constraint ) {

		if ( $this->generator->checkInDB( $value ) ) {
			$this->context->buildViolation( $constraint->message )
			              ->setParameter( '%string%', $value )
			              ->addViolation();
		}
	}
}