var resultData;
function shortenUrl(data) {
    $.ajax({
        method: 'POST',
        url: shorten_url,
        data: data,
        beforeSend: function () {
            $('.errors').html("");
            $('.result').val("");
            $('.edit-error').remove("");
            $('.domain-url').remove();
            $('.alert-success').remove();
            $('.result').prop('readonly', true);
            $('.edit-url').text("Edit");
        }
    }).done(function (result) {
        resultData = result;
        showResult(result);
        $('.edit-url').prop('disabled', false);
        $('.make-immo').prop('disabled', false);
        if (result.errors == 0)
            $('.panel-body').append("<div class='alert alert-success' role='alert'>Short Url have been created. But if you don't do your short url immortal, it will work 2 weeks.</div>");

    });
}

function editUrl(data) {

    $.ajax({
        method: 'POST',
        url: edit_url,
        data: data,
        beforeSend: function () {
            $('.edit-error').remove("");
            $('.edit-url').prop('disabled', true);
            $('.make-immo').prop('disabled', true);
        }
    }).done(function (result) {
        resultData = result;
        //console.log(result.errors)
        if (result.errors > 0) {
            showResult(result, "EDIT");

        } else {
            $('.domain-url').remove();
            $('.edit-url').text("Edit");
            $('.result').prop('readonly', true);

            showResult(result, "EDIT");
        }
        $('.edit-url').prop('disabled', false);
        $('.make-immo').prop('disabled', false);
    });
}

function makeImmorrtal(data) {
    $.ajax({
        method: 'POST',
        url: make_immo,
        data: data,
        beforeSend: function () {
            //$('.edit-error').remove("");
            $('.edit-url').prop('disabled', true);
            $('.make-immo').prop('disabled', true);
        }
    }).done(function (result) {
        resultData = result;
        $('.alert-success').text("Immortalized");
        if ($('.domain-url')) {
            $('.domain-url').remove();
            showResult(result, "EDIT");
            $('.result').prop('readonly', true);

        }
    });
}

function showResult(result, flag) {
    if (result.errors === 0) {
        $('.result').val(document.domain + "/" + result.data.short_url);
    } else {
        if (flag != "EDIT") {
            for (var i = result.errors - 1; i >= 0; i--) {
                $('.errors').append("<div class='alert alert-danger' role='alert'>" + result['data'][i]['message'] + "</div>");
            }
        } else if (flag == "EDIT") {
            $('.edit-url').prop('disabled', false);
            for (var i = result.errors - 1; i >= 0; i--) {
                console.log("Fuck of");
                $(".table tbody").append("<tr class='edit-error error-" + i + "'></tr>");
                $(".table tbody tr.error-" + i).append("<div class='alert alert-danger' role='alert'>" + result['data'][i]['message'] + "</div>");

            }
        }
    }
}

$(document).ready(function () {

    //Shorten url
    $('#shorten').click(function (event) {
        event.preventDefault();
        shortenUrl({url: origin_url.value});
    });

    //Edit url
    $('.edit-url').click(function (event) {
        event.preventDefault();
        if ($('.edit-url').text() == 'Edit') {
            $('.edit-url').text("Send");
            $('.result').prop('readonly', false);
            $('.result').val(resultData['data']['short_url']);
            $('.result').before("<span class='input-group-addon domain-url' id='basic-addon3'>" + document.domain + "/</span>");

        } else if ($('.edit-url').text() == 'Send') {
            if (resultData.errors > 0) {
                editUrl({short_url: short_url.value, id: resultData['id']});
            } else {
                editUrl({short_url: short_url.value, id: resultData['data']['id']});
            }
        }
    });

    //Make url ommortal
    $('.make-immo').click(function (event) {
        event.preventDefault();
        makeImmorrtal({id: resultData['data']['id']});
    });

    var clipboard = new Clipboard('.copy');

    clipboard.on('success', function (e) {
        console.info('Action:', e.action);
        console.info('Text:', e.text);
        console.info('Trigger:', e.trigger);

        e.clearSelection();
    });

    clipboard.on('error', function (e) {
        console.error('Action:', e.action);
        console.error('Trigger:', e.trigger);
    });
});

