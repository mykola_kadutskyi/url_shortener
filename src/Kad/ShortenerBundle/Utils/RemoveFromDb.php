<?php
namespace Kad\ShortenerBundle\Utils;

use Kad\ShortenerBundle\Entity\Stats;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class RemoveFromDb extends Controller {

	protected $container;

	private $date;

	public function __construct( Container $container ) {
		$this->container = $container;
		$this->date      = new \DateTime();

	}


	public function checkAndRemoveFromDb() {
		$em    = $this->getDoctrine()->getManager();
		$pairs = $em->getRepository( 'KadShortenerBundle:UrlPair' )
		            ->findAll();
		for ( $i = count( $pairs ) - 1; $i >= 0; $i -- ) {

			if ( $pairs[ $i ]->getRemoveAt() < $this->date ) {

				$stat = new Stats();
				$stat->setAction( "deleted" );
				$stat->setPairId( $pairs[ $i ]->getId() );
				$em->persist( $stat );
				$em->remove( $pairs[ $i ] );

			}

		}
		$em->flush();
//		return $pairs;
	}
}