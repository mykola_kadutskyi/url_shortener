<?php
namespace Kad\ShortenerBundle\Utils;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class GenerateString extends Controller {

	protected $container;

	private $string;

	public function __construct( Container $container ) {
		$this->container = $container;
		$this->string    = $this->container->getParameter( 'kad_url_shortener.string' );

	}

	/**
	 * @return string
	 *
	 * random string generated from characters and with length defined in config.yml
	 */
	public function generate() {
		$chars    = $this->string['chars'];
		$charsLen = strlen( $chars );
		$string   = '';
		for ( $i = 0; $i < $this->string['length']; $i ++ ) {
			$string .= $chars[ rand( 0, $charsLen - 1 ) ];
		}

		return $string;
	}

	public function checkInDB( $urlString ) {
		$urlPair = $this->getDoctrine()
		                ->getRepository( 'KadShortenerBundle:UrlPair' )
		                ->findBy( array( 'shortUrl' => $urlString ) );
		if ( $urlPair ) {
			return true;
		}

		return false;
	}
}