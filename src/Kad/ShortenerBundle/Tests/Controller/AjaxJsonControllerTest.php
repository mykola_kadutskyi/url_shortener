<?php

namespace Kad\ShortenerBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AjaxJsonControllerTest extends WebTestCase
{
    public function testShortenurl()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/shortenUrl');
    }

    public function testEditurl()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/editUrl');
    }

    public function testMakeurlimmortal()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/makeUrlImmortal');
    }

}
